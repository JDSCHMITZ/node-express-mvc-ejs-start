# Node Express MVC EJS Bootstrap Starter app

## Overview 

This app allows users to navigate through banking details such as account, transaction, organization, and category details. This app also allows users to interact with database and perform data modification. 

## Team
- Nathan Meeker
-- Responsible for Category
- Kenneth Grannas 
-- Responsible for Organization
- Joshua Schmitz
-- Responsible for Account
- Chulho Yoon
-- Responsible for Transaction

## Links

- Repository <https://bitbucket.org/JDSCHMITZ/node-express-mvc-ejs-start/src/master> 

- Heroku deployed site <https://warm-taiga-28101.herokuapp.com/>

## Review Code Organization

- app.js - Starting point for the application. Defines the express server, requires routes and models. Loads everything and begins listening for events.
- controllers/ - logic for handling client requests
- data/ - seed data loaded each time the application starts
- models/ - schema descriptions for custom data types
- routes/ - route definitions for the API
- utils/ - utilities for logging and seeding data
- views/ - EJS - embedded JavaScript and HTML used to create dynamic pages


*Open browser to the location displayed, e.g. http://localhost:8089/*