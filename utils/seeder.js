// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const transactions = require('../data/transactions.json')
const organizations = require('../data/organizations.json')
const categories = require('../data/categories.json')
const accounts = require('../data/accounts.json')
// const products = require('../data/products.json')
// const orders = require('../data/orders.json')
// const orderLineItems = require('../data/orderLineItems.json')
// const puppies = require('../data/puppies.json')

// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // transactions don't depend on anything else...................

  db.transactions = new Datastore()
  db.transactions.loadDatabase()

  // insert the sample data into our data store
  db.transactions.insert(transactions)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.transactions = db.transactions.find(transactions)
  LOG.debug(`${app.locals.transactions.query.length} transactions seeded`)

 // Products don't depend on anything else .....................

 db.organizations = new Datastore()
 db.organizations.loadDatabase()

 // insert the sample data into our data store
 db.organizations.insert(organizations)

 // initialize app.locals (these objects will be available to our controllers)
 app.locals.organizations = db.organizations.find(organizations)
 LOG.debug(`${app.locals.organizations.query.length} organizations seeded`)

  // Each Categories needs a product and an order beforehand ...................

  db.categories = new Datastore()
  db.categories.loadDatabase()

  // insert the sample data into our data store
  db.categories.insert(categories)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.categories = db.categories.find(categories)
  LOG.debug(`${app.locals.categories.query.length} categories seeded`)

  // accounts don't depend on anything else...................

  db.accounts = new Datastore()
  db.accounts.loadDatabase()
  
  // insert the sample data into our data store
  db.accounts.insert(accounts)
  
  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} accounts seeded`)

  // extra...

  // db.puppies = new Datastore()
  // db.puppies.loadDatabase()

  // // insert the sample data into our data store
  // db.puppies.insert(puppies)

  // // initialize app.locals (these objects will be available to our controllers)
  // app.locals.puppies = db.puppies.find(puppies)
  // LOG.debug(`${app.locals.puppies.query.length} puppies seeded`)

  LOG.info('END Seeder. Sample data read and verified.')
}
