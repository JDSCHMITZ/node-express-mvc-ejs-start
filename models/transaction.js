/** 
*  Transaction model
*  Describes the characteristics of each attribute in an Transaction item - one entry on a customer's transaction.
*
* @author Chulho Yoon <s521066@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')
const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionType: { type: String, required: true },
  amount: {type: Number, required: true},
})
module.exports = mongoose.model('Transaction', TransactionSchema)
