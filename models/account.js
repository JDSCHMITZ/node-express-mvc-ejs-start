/** 
*  Account model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Josh Schmitz
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrderSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  AccountType: { type: String, required: true, unique: true, default: 'checking'},
  AccountID: { type: Number, required: true, unique: true, default: 1 },
  TotalOrderNum: { type: Number, required: true, unique: true, default: 0},
  DateCreated: { type: String, required: true, default: '3/18/2019' },
})

module.exports = mongoose.model('Order', OrderSchema)