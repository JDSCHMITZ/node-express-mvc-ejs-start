/** 
*  organization model
*  Describes the characteristics of each attribute in a organization resource.
*
* @author Kenny Grannas <s526521@nwmissouri.com>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrganizationSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  email: { type: String, required: true, unique: true },
  organization: { type: String, required: true, default: 'Organization' },
  street1: { type: String, required: true, default: 'Street 1' },
  street2: { type: String, required: false, default: '' },
  city: { type: String, required: true, default: 'Maryville' },
  state: { type: String, required: true, default: 'MO' },
  zip: { type: String, required: true, default: '64468' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('organization', OrganizationSchema)
